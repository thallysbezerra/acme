$( document ).ready(function() {

    $.getJSON("https://randomuser.me/api/?results=10", function(data) {
        for(var contact = 0; contact < data.results.length; contact++) {
          $(".content").append('<div class="card"><div><img src="' + data.results[contact].picture.large + '" class="card__img"></div><div><p class="card__name">' + data.results[contact].name.title + ' ' + data.results[contact].name.first + ' ' + data.results[contact].name.last + '</p><p class="card__email">' + data.results[contact].email + '</p><p class="card__phone">' + data.results[contact].phone + '</p></div></div>');
        }
      });

});