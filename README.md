# Stack de desenvolvimento

- Editor: VS Code
- Extração de assets: Photoshop CC
- Linguagem de marcação: HTML5
- Pré-processador CSS: SASS/SCSS

# Tecnologias e conceitos

- API importada via método getJSON, usando jQuery com append
- CSS BEM (Block Element Modifier)
- CSS Grid para estruturas externas
- CSS Flexbox para estruturas internas
- Mobile-first
- Não foi utilizado nenhum Framework CSS
- Propriedades CSS em ordem alfabética
- SASS/SCSS 
	- Compilação de .scss via Ruby Devkit
	- Variáveis para cores

# Instruções para rodar o projeto

- Baixar e executar o `index.html`, sem necessidade de servidor local